var scripts = document.getElementsByTagName("script");
var thisScript = scripts[scripts.length-1];
var thisScriptsSrc = thisScript.src.replace(/\?.*/, '').replace(/\/[^\/]*$/, '');
var WORKER_PATH = thisScriptsSrc + '/recorderWorker.js';

function AudioRecord(conf) {
	window.AudioContext = window.AudioContext ||
	window.webkitAudioContext ||
	window.mozAudioContext ||
	window.msAudioContext;

	navigator.getUserMedia = navigator.getUserMedia ||
	navigator.webkitGetUserMedia ||
	navigator.mozGetUserMedia ||
	navigator.msGetUserMedia;

	var config = conf || {};
	var microphone, node;
	if(AudioContext) {
		var audio_context = new AudioContext;
	} else {
		throw new AudioRecordException('AudioRecord.js not supported');
	}

	var recording = false;
	var bufferLen = config.bufferLength || 4096;

	//	Create worker
	var worker = new Worker(WORKER_PATH);

	worker.onmessage = function(e) {
		try {
			postAudio(e.data);
		} catch (ex) {
			console.log(ex);
		}
	}

	// No callback defined
	if(!config.url) {
		throw new AudioRecordException("Callback must be specified in config");
	}

	// Browser too old
	if (!navigator.getUserMedia) {
		throw new AudioRecordException("Browser does not support getUserMedia");
	}

	// Kick things off!
	navigator.getUserMedia(
		{ audio: true }, 
		function(stream) { init(stream) }, 
		function(e) { throw e; }
	);

	// Set audio stream up
	function init(stream) {
		microphone = audio_context.createMediaStreamSource(stream);
		
		worker.postMessage({
			command: 'init',
			config: {
				sampleRate: microphone.context.sampleRate
			}
		});

		node = (microphone.context.createScriptProcessor || microphone.context.createJavaScriptNode).call(microphone.context, bufferLen, 2, 2);

		node.onaudioprocess = function(e){
			if (!recording) return;
			worker.postMessage({
				command: 'record',
				buffer: [
					e.inputBuffer.getChannelData(0),
					e.inputBuffer.getChannelData(1)
				]
			});
		}

		// Necessary if node is a ScriptProcessorNode on Webkit
		// https://code.google.com/p/chromium/issues/detail?id=82795
		node.connect(audio_context.destination);
		microphone.connect(node);

		levels_node = audio_context.createScriptProcessor(2048 , 1, 1);

		analyser = audio_context.createAnalyser();
		analyser.smoothingTimeConstant = 0.2;

		microphone.connect(analyser);
		analyser.connect(levels_node);

		// Necessary if node is a ScriptProcessorNode on Webkit
		// https://code.google.com/p/chromium/issues/detail?id=82795
		levels_node.connect(audio_context.destination);

		var max_vol = 0;

		levels_node.onaudioprocess = function(event) {
			var buf = event.inputBuffer.getChannelData(0);
			var len = buf.length;
			var rms = 0;

			// Iterate through buffer
			for (var i = 0; i < len; i++) 
			{
				rms += Math.abs(buf[i]);
			}
			rms = Math.sqrt(rms / len);

			if(config.levels_callback)
				config.levels_callback(rms);
		}
	}

	this.setUrl = function(url) {
		config.url = url;
	}

	// Start recording
	this.start = function() {
		if(recording) throw new AudioRecordException("Already recording");
		recording = true;
	}

	// Pause recording
	this.pause = function() {
		recording = false;
	}

	// Stop recording and call callback
	this.stop = function(post) {
		recording = false;
		if(post || typeof(post) == 'undefined') {
			worker.postMessage({
				command: 'exportWAV',
				type: 'audio/wav'
			});
		}
		this.clearBuffer();
	}

	// Clear audio buffer
	this.clearBuffer = function() {
		worker.postMessage({ command: 'clear' });		
	}

	// THIS NEEDS TO BE COMMENTED PROPERLY, ALSO postAudio SHOULD BE RENAMED
	postAudio = config.postAudio || function(blob) {
		if(blob.size <= 44) throw new AudioRecordException("Buffer is empty!");
		//var fileName = new Date().toISOString() + '.wav';
		var formData = new FormData();
		//formData.append('audio-filename', fileName);
		formData.append('audio-blob', blob);

		var request = new XMLHttpRequest();
		request.onreadystatechange = function () {
			if (request.readyState == 4 && request.status == 200) {
				config.callback(request.responseText);
			}
		};
		request.open('POST', config.url);
		request.send(formData);
	}

	// Our very own exception!
	function AudioRecordException(message) {
		this.message = message;
		this.name = "AudioRecordException";
	}

	// Without this everything is ugly.
	return this;
}
