Example usage:
=============

	var audioRecorder = new AudioRecord({    
		url: "/record",    
		callback: function(fileUrl) {   
			dialog.setValueOf('Upload', 'txtUrl', fileUrl);    
		},     
		levels_callback: function(level) {     
			var level_bar = document.getElementById('level_bar');   
			if(level_bar) {     
				level_bar.style.height = (20 - (20 * level)) + "px";    
			}    
		}     
	});


To start/stop recording:

	audioRecorder.start();
	audioRecorder.pause();
	audioRecorder.stop();
